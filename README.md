# <a href="cyber-secu.space">YoursMask</a>

## I. Description

<img src="https://cdn.discordapp.com/attachments/710386691730309130/710460206752202813/Yourmask.png" width="33%">

* Le site <a href="cyber-secu.space">YoursMask</a> a pour but d'aider les habitants de Bordeaux dans l'achat ou la vente de masque.
Le site permet de recenser le nombre de masque restant dans les commerces les plus proches. Il y a un système de réservation pour aider à la distribution.


## II. Technologie utilisée
* Back, framework PHP : 
    * <a href="https://laravel.com/">Laravel</a>, l'un des frameworks les plus utilisés avec une documentation fournie.
    * Base de donnée MariaDB
    * <details><summary>La base</summary><img src="https://cdn.discordapp.com/attachments/582825013690892290/710780724029947954/unknown.png" </details>
</br>

* Front, framework CSS : 
    * <a href="https://getbootstrap.com/">Bootstrap</a>, un framework rapide (parfait pour les conditions) avec une bonne documentation.
    * Bootswatch Lux


</br>

## III. Maquettes

<details>
<summary>Pages d'accueil</summary>
<img src="https://cdn.discordapp.com/attachments/710386691730309130/710463040201424966/unknown.png">
</details>

<details>
<summary>Liste des points de vente avec la localisation et le stock</summary>
<img src="https://cdn.discordapp.com/attachments/710386691730309130/710464535810670672/unknown.png">
</details>

<details>
<summary>Les informations de l'utilisateur avec ses réservations</summary>
<img src="https://cdn.discordapp.com/attachments/710386691730309130/710474798601601075/unknown.png">
</details>

<details>
<summary>Les informations d'un professionnel avec les réservations effectuées par des utilisateurs pour son établissement</summary>
<img src="https://cdn.discordapp.com/attachments/710386691730309130/710484395642847302/unknown.png">
</details>



## IV. Fonctionnalités 

* Fonctionnalités générales :

    * Affichage des points de vente via une carte
    * Itinéraire pour se rendre à ces points de vente
    * Création d'utilisateur client ou professionel

* Côté client :
    
    * Possibilité de réserver des masques.
    * De supprimer ses réservations
    * Modification des informations

* Côté professionnel :

    * Ajout d'un établissement(point de vente)
    * Modification des informations (nombre de masque en stock)
    * Liste des clients avec le nombre de masques commandés
    * Décrémentation du stock à chaque commande
    * Un limite maximum de masque par personne modulable

## V. Installation en local
* Prérequis :

    * PHP 7
    * MySql
    * Composer
    * Laravel

* Tout d'abord il faut cloner le dépôt.
* Créer un fichier .env dans le dossier /chall_48h et copier le texte suivant:
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=masksbdx
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
* Lancer les commandes suivantes :
```
composer install
```
```
npm install
```
```
php artisan key:generate
```
```
php artisan migrate
```

* Pour allumer le serveur laravel la commande est la suivante :
```
php artisan serve
```
* L'adresse du serveur est par défault 127.0.0.1:8000


## VI. Auteur
* SALLES Sacha - <a href="https://gitlab.com/Sascha_40">Sascha40</a>
* LEVEIL Florian - <a href="https://github.com/FlorianLeveil">FlorianLeveil</a>
* DELBREL Antoine - <a href="https://gitlab.com/antoinedelbrel">antoinedelbrel</a>
* REGNAULT Quentin - <a href="https://github.com/RegnaultQuentin">RegnaultQuentin</a>


</br>
<h2><a href="cyber-secu.space">Yourmask</a><h2>