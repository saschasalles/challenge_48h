@extends('layouts.app')

@section('content')

<div class="container justify-content-around mt-3">
    <div class="row">

    <div class="map col-md-6 col-sm-12 mt-5">
        <h2 class="mb-5 h2">Bienvenue sur YourMask, commandez vos masques Anti-Covid 19 dans les magasins les plus proches de chez vous.</h2>
        @if (Auth::check())
        <a class="btn btn-primary rounded-pill" href="{{ route('crud_user.index') }}">Voir mes réservations</a>
        @else
        <a class="btn btn-primary rounded-pill" href="{{ route('register') }}">Créer un compte</a>
        @endif
    </div>

    <div class="map col-md-6 col-sm-5 d-none d-md-block">
            {{-- <img src="{{ asset('images/bordeaux.png') }}" alt="map" class="bordeaux"> --}}
            <iframe frameborder="0" height=450 class="rounded w-100" src="https://www.google.com/maps/embed/v1/view?key={{ env('GOOGLE_MAPS_API_KEY') }}&center=44.8433, -0.5693&zoom=15&language=fr" allowfullscreen>
            </iframe>
        </div>
        
    
    </div>
</div>
{{-- <iframe width="600" height="450" frameborder="0" style="border-radius: 20px" src="https://www.google.com/maps/embed/v1/view?key={{ env('GOOGLE_MAPS_API_KEY') }}&center=-33.8569,151.2152&zoom=18&maptype=satellite" allowfullscreen>
</iframe> --}}

    <hr class="mt-5 w-50">

    <div class="container mt-5">
        <h2 class="h2 text-center">Tous nos points de ventes</h2>
        
        <div class="row row-cols-1 row-cols-md-3">
            @foreach ($etabs as $etab)

            <div class="col mb-4">
                <div class="card h-100 border-0 shadow">
                    <iframe frameborder="0" height=250 class="card-img-top" src="https://www.google.com/maps/embed/v1/place?key={{ env('GOOGLE_MAPS_API_KEY') }}&q={{$etab -> adresse." ".$etab -> code." ".$etab -> ville}}" allowfullscreen>
                    </iframe>
                    <div class="card-body">
                        <h5 class="card-title">{{$etab->name}}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{$etab->nb_mask}}</h6>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
@endsection