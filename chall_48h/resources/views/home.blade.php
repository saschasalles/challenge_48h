@extends('layouts.app')

@section('content')

<div class="container">
    <div class="table-responsive shadow rounded">
        <table class="table table-borderless tab">
            <thead class="thead-dark">
                <tr>
                <th scope="col">Point de vente</th>
                <th scope="col">Masques Disponibles</th>
                <th scope="col">Localisation</th>
                <th scope="col">Reservation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($etabs as $etab)
                <tr>
                    <th scope="row" class="align-middle">{{$etab->name}}</th>
                    <td class="align-middle">{{$etab->nb_masks}}</td>
                    <td class="align-middle"><a class="btn btn-primary rounded-pill" data-toggle="modal" href="#map{{$etab->id}}">Localisation</a></td>
                    <td class="align-middle"><a class="bouton btn btn-primary rounded-pill" data-toggle="modal" href="#info{{$etab->id}}">Réserver</a></td>

                </tr>

                <div id="map{{$etab->id}}" class="modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('home.store') }}" method="post">
                                @csrf
                                <div class="modal-header">			
                                    <h4 class="modal-title">{{$etab->name}}</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">	
                                    <iframe frameborder="0" height=450 class="rounded w-100" src="https://www.google.com/maps/embed/v1/place?key={{ env('GOOGLE_MAPS_API_KEY') }}&q={{$etab -> adresse." ".$etab -> code." ".$etab -> ville}}" allowfullscreen>
                                    </iframe>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        
                <div id="info{{$etab->id}}" class="modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('home.store') }}" method="post">
                                @csrf
                                <div class="modal-header">			
                                    <h4 class="modal-title">Réserver</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">					
                                    <div class="form-group">
                                        <select class="custom-select rounded-pill" name="nb_masks_take" id="nb_masks_take">
                                            <option selected>Nombre de masques:</option>
                                            @for ($i = 0; $i < $etab->max_masks; $i++)
                                            <option value="{{$i + 1}}">{{$i + 1}}</option>
                                            @endfor
                                        </select>
                                    </div>                      
                                    <div class="form-group">
                                        <label>Date de recuperation</label>
                                        <input type="date" name="date_take" class="form-control rounded-pill">
                                        <input type="text" name="id_etab" class="form-control d-none" value="{{$etab->id}}">
                                    </div>              
                                </div>
                                <div class="modal-footer">
                                    <input type="button" class="btn btn-light rounded-pill" data-dismiss="modal" value="Annuler">
                                    <input type="submit" class="btn btn-success rounded-pill" value="Réserver">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
                <tr>
            </tbody>
        </table>
    </div>


</div>

@endsection