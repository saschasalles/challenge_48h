@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="mt-3 text-center h2">Mon compte</h2>
    <div class="content mt-3">
        <div class="col-lg-12">
            <h4 class="pb-2">Mes informations</h4>
            <hr>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h5>Nom:</h5>
                </div>
                <div class="col-lg-6 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$user->name}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h5>Mail:</h5>
                </div>
                <div class="col-lg-6 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$user->email}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h5>Ville :</h5>
                </div>
                <div class="col-lg-6 pt-2">
                    <h6 class="btn btn-outline-primary rounded">{{$user->ville}}</h6>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h5>Code postal :</h5>
                </div>
                <div class="col-lg-6 pt-2">
                    <h6 class="btn btn-outline-primary rounded">{{$user->code}}</h6>
                </div>
                <div class="col-lg-6 pt-4">
                    <h5>Adresse :</h5>
                </div>
                <div class="col-lg-6 pt-2">
                    <h6 class="btn btn-outline-primary rounded">{{$user->adresse}}</h6>
                </div>
            </div>
            <div class="col-lg-12 text-center pt-5">
                <a href="#infoModal" class="btn-primary btn rounded-pill" data-toggle="modal">Modifier mes informations</a>
            </div>
        </div>
    </div>

    <div id="infoModal" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('crud_user.update', $user->id) }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">			
                        <h5 class="modal-title">Modifier vos informations</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <div class="form-group">
                            <label>Prénom</label>
                            <input type="text" name="name" class="form-control rounded-pill" value="{{$user->name}}">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control rounded-pill" value="{{$user->email}}">
                        </div>
                        <div class="form-group">
                            <label>Ville</label>
                            <input type="text" name="ville" class="form-control rounded-pill" value="{{$user->ville}}">
                        </div>    
                        <div class="form-group">
                            <label>Code</label>
                            <input type="text" name="code" class="form-control rounded-pill" value="{{$user->code}}">
                        </div>   
                        <div class="form-group">
                            <label>Adresse</label>
                            <input type="text" name="adresse" class="form-control rounded-pill" value="{{$user->adresse}}">
                        </div>      
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-light rounded-pill" data-dismiss="modal" value="Annuler">
                        <input type="submit" class="btn btn-success rounded-pill" value="Modifier">
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="row justify-content-center">
        
            <div class="col-12">
                <h5 class="text-center mt-5 mb-5">Mes réservations</h5>
                <div class="table-responsive">
                    @if(!$commandes->isEmpty())
                        <table class="table table-borderless tab">
                            <thead class="thead-dark">
                                <tr>
                                <th scope="col">Point de vente</th>
                                <th scope="col">Quantité</th>
                                <th scope="col">Adresse</th>
                                <th scope="col">Itinéraire</th>
                                <th scope="col">Réservation</th>
                                <th scope="col">Annuler</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for ($i = 0; $i < $nb_commandes; $i++)
                                    <tr>
                                        <td scope="row" class="align-middle">{{$etablissement[$i]->name}}</td>
                                        <td class="align-middle">{{$commandes[$i]->nb_masks_take}}</td>
                                        <td class="align-middle">{{$etablissement[$i]->adresse." ".$etablissement[$i]->ville." ".$etablissement[$i]->code}}</td>
                                        <td class="align-middle"><a class="btn btn-primary rounded-pill" data-toggle="modal" href="#map{{$etablissement[$i]->id}}">Itinéraire</a></td>
                                        <td class="align-middle">{{$commandes[$i]->date_take}}</td>
                                        <td class="align-middle">
                                            <form class="" action="{{ route('crud_user.destroy', ['crud_user' => $commandes[$i]->id]) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger rounded-pill">Annuler</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <div id="map{{$etablissement[$i]->id}}" class="modal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <form action="{{ route('home.store') }}" method="post">
                                                    @csrf
                                                    <div class="modal-header">			
                                                        <h4 class="modal-title">Itinéraire de chez vous à {{$etablissement[$i]->name}}</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    </div>
                                                    <div class="modal-body">	
                                                        <iframe frameborder="0" height=450 class="rounded w-100" src="https://www.google.com/maps/embed/v1/directions?key={{ env('GOOGLE_MAPS_API_KEY') }}&origin={{$user->adresse." ".$user->code." ".$user->ville}}&destination={{$etablissement[$i] -> adresse." ".$etablissement[$i] -> code." ".$etablissement[$i] -> ville}}" allowfullscreen>
                                                        </iframe>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                            </tbody>
                        </table>
                    @else
                        <div class="text-center">Vous n'avez aucune commandes en cours</div>
                    @endif
                </div>
            </div> 
        
            
    </div>
</div>

@endsection