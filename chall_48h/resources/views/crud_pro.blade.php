@extends('layouts.app')

@section('content')

@if (empty($societe->id))

<div class="container-fluid">
    <div class="jumbotron text-center">
        <h1 class="display-4">Bonjour !</h1>
        <p class="lead">Commencer par ajouter votre entreprise</p>
        <a class="btn btn-success rounded-pill" href="#addModal" data-toggle="modal" role="button">Ajouter ma societé</a>
    </div>


    <div id="addModal" class="modal">
        <div class="modal-dialog">
            <div class="modal-content border-0 rounded">
                <form action="{{ route('crud_pro.store') }}" method="post">
                    @csrf
                    <div class="modal-header">			
                        <h4 class="modal-title">Ajouter votre societé</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" name="name" class="form-control rounded-pill" required>
                        </div>
                        <div class="form-group">
                            <label>Adresse</label>
                            <input type="text" name="adresse" class="form-control rounded-pill" required>
                        </div>
                        <div class="form-group">
                            <label>Code postale</label>
                            <input type="text" name="code" class="form-control rounded-pill" required>
                        </div>
                        <div class="form-group">
                            <label>Ville</label>
                            <input type="text" name="ville" class="form-control rounded-pill" required>
                        </div> 
                        <div class="form-group">
                            <label>Nombre de masque disponible</label>
                            <input type="number" name="nb" class="form-control rounded-pill" required>
                        </div> 
                        <div class="form-group">
                            <label>Nombre maximum de masques par clients</label>
                            <input type="number" name="max" class="form-control rounded-pill" required>
                        </div>                              
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-light rounded-pill" data-dismiss="modal" value="Annuler">
                        <input type="submit" class="btn btn-success rounded-pill" value="Ajouter">
                    </div>
                </form>
            </div>
        </div>
    </div>

@else

<div class="container">
    <div class="row">
        <div class="col-lg-12 pt-2">
            <h2 class="pb-2">Mon Compte Professionel</h2>
            <hr>
        </div>
    </div>

    <div class="row justify-content-around">
        <button class="rounded btn btn-outline-success text-center mt-1">
            Stock Total : {{$societe->nb_masks}}
        </button>
        <div class=" rounded btn btn-outline-success text-center mt-1">
            Nombre de réservation : {{$nb_reservation}}
        </div>
            <hr class="w-100">
    </div>

        <div class="col-lg-12 pt-2">
            <h2 class="pb-2">Masques Réservés</h2>
        </div>
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Nom du client</th>
                            <th scope="col">Quantité</th>
                            <th scope="col">Date de retrait</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($reservation as $reserv)
                        <tr class="table-primary">
                        <td>{{$reserv->id_user}}</td>
                            <td>{{$reserv->nb_masks_take}}</td>
                            <td>{{$reserv->date_take}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table> 
            </div>
        </div>

    <div class="row pt-5 pb-5">
        <div class="col-lg-12">
            <h2 class="pb-2">Mes informations</h2>
            <hr>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h4>Prénom:</h4>
                </div>
                <div class="col-lg-6 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$user->name}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h4>Mail:</h4>
                </div>
                <div class="col-lg-6 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$user->email}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h4>Adresse:</h4>
                </div>
                <div class="col-lg-6 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$user->adresse}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h4>Ville:</h4>
                </div>
                <div class="col-lg-6 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$user->ville}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-6 pt-4">
                    <h4>Code Postale:</h4>
                </div>
                <div class="col-lg-6 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$user->code}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-12 text-center pt-5">
            <a href="#infoModal" class="btn-primary btn rounded-pill" data-toggle="modal">Modifier mes informations</a>
        </div>
    </div>

    <div class="row pt-5">
        <div class="col-lg-12">
            <h2 class="pb-2">Informations du commerce</h2>
            <hr>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-8 pt-4">
                    <h4>Nom du commerce:</h4>
                </div>
                <div class="col-lg-4 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$societe->name}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-8 pt-4">
                    <h4>Maximum de masque par commande:</h4>
                </div>
                <div class="col-lg-4 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$societe->max_masks}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-8 pt-2">
                    <h4>Nombre de masque en stock à l'instant T:</h4>
                </div>
                <div class="col-lg-4 pt-2">
                    <h5 class="btn btn-outline-primary rounded">15600</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-8 pt-3">
                    <h4>Nombre de masque en comptant les commandes:</h4>
                </div>
                <div class="col-lg-4 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$societe->nb_masks}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-7 col-md-7 pt-2">
                    <h4>Adresse:</h4>
                </div>
                <div class="col-lg-4 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$societe->adresse}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-8 pt-2">
                    <h4>Ville:</h4>
                </div>
                <div class="col-lg-4 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$societe->ville}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pt-2">
            <div class="row">
                <div class="col-lg-8 pt-3">
                    <h4>Code postale:</h4>
                </div>
                <div class="col-lg-4 pt-2">
                    <h5 class="btn btn-outline-primary rounded">{{$societe->code}}</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-12 text-center pt-5">
            <a href="#socModal" class="btn-primary btn rounded-pill" data-toggle="modal">Modifier les informations du commerce</a>

        </div>
    </div>
</div>

<div id="infoModal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('crud_user.update', $user->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="modal-header">			
                    <h4 class="modal-title">Modifier vos informations</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">					
                    <div class="form-group">
                        <label>Prénom</label>
                        <input type="text" name="name" class="form-control rounded-pill" value="{{$user->name}}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control rounded-pill" value="{{$user->email}}">
                    </div>
                    <div class="form-group">
                        <label>Adresse</label>
                        <input type="text" name="adresse" class="form-control rounded-pill" value="{{$user->adresse}}">
                    </div>                      <div class="form-group">
                        <label>Ville</label>
                        <input type="text" name="ville" class="form-control rounded-pill" value="{{$user->ville}}">
                    </div>
                    <div class="form-group">
                        <label>Code postal</label>
                        <input type="text" name="code" class="form-control rounded-pill" value="{{$user->code}}">
                    </div>              
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-light rounded-pill" data-dismiss="modal" value="Annuler">
                    <input type="submit" class="btn btn-success rounded-pill" value="Modifier">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="socModal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('crud_pro.update', $societe->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="modal-header">			
                    <h4 class="modal-title">Modifier les informations du commerce</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">					
                    <div class="form-group">
                        <label>Nom du commerce</label>
                        <input type="text" name="name" class="form-control rounded-pill" value="{{$societe->name}}">
                    </div>
                    <div class="form-group">
                        <label>Ajouter des masques au stock</label>
                        <input type="number" name="add_masks" class="form-control rounded-pill" value="0">
                    </div>      
                    <div class="form-group">
                        <label>Suprimer des masques du stock</label>
                        <input type="number" name="sup_masks" class="form-control rounded-pill" value="0">
                    </div> 
                    <div class="form-group">
                        <label>Maximum de masques par commande</label>
                        <input type="number" name="max_masks" class="form-control rounded-pill" value="{{$societe->max_masks}}">
                    </div>   
                    <div class="form-group">
                        <label>Adresse</label>
                        <input type="text" name="adresse" class="form-control rounded-pill" value="{{$societe->adresse}}">
                    </div>
                    <div class="form-group">
                        <label>Ville</label>
                        <input type="text" name="ville" class="form-control rounded-pill" value="{{$societe->ville}}">
                    </div>
                    <div class="form-group">
                        <label>Code postale</label>
                        <input type="text" name="code" class="form-control rounded-pill" value="{{$societe->code}}">
                    </div> 
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-light rounded-pill" data-dismiss="modal" value="Annuler">
                    <input type="submit" class="btn btn-success rounded-pill" value="Modifier">
                </div>
            </form>
        </div>
    </div>
</div>



@endif

@endsection