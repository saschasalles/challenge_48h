<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    protected $fillable = [
        'date_take', 'nb_masks_take', 'id_user', 'id_etablissement',
    ];
}
