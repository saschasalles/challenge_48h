<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Commande;
use App\Etablissement;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $etab = Etablissement::get();
        return view('home',['etabs' => $etab]);

    }

    public function store(Request $request)
    {
        $user = auth()->user();
        $etab = Etablissement::where('id', $request->id_etab)->first();

        $etab->nb_masks = $etab->nb_masks - $request->nb_masks_take;
        $etab->save();

        Commande::create([
            'date_take' => $request->date_take,
            'nb_masks_take' => $request->nb_masks_take,
            'id_user' => $user->id,
            'id_etablissement' => $request->id_etab,

        ]);
        return back()->with("success");
    }

    public function destroy($id)
    {
        //
    }

}
