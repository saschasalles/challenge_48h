<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Commande;
use App\Etablissement;

class Crud_userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $commandes = Commande::where('id_user', $user->id)->get();
        $etablissement = [];
        if (!empty($commandes)) {
            foreach ($commandes as $commande){
                $etab = Etablissement::get()->where('id', $commande->id_etablissement)->first();
                array_push($etablissement, $etab);
            }
            $nb_commandes = count($commandes);
            return view('crud_user',['user' => $user, 'commandes' => $commandes, 'etablissement' => $etablissement, 'nb_commandes' => $nb_commandes]);
        }
        return view('crud_user',['user' => $user, 'commandes' => $commandes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $compte = User::find($id);
        $commandes = Commande::find($id);
        return view('crud_user');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mon_compte = User::find($id);
        $mon_compte->name = $request->name;
        $mon_compte->email = $request->email;
        $mon_compte->ville = $request->ville;
        $mon_compte->code = $request->code;
        $mon_compte->adresse = $request->adresse;
        $mon_compte->save();

        return back()->with("success");
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $commande = Commande::find($id)->first();
        $etablissement = Etablissement::where('id', $commande->id_etablissement)->first();

        $etablissement->nb_masks = $etablissement->nb_masks + $commande->nb_masks_take;
        $etablissement->save();

        $commande->delete();
        return redirect()->route('crud_user.index');

    }
}
