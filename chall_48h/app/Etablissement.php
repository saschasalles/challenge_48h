<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etablissement extends Model
{
    protected $fillable = [
        'name', 'lat', 'long', 'nb_masks', 'id_user', 'max_masks','ville','adresse','code'
    ];
}

